# ANSIBLE - Industrialiser les déploiements - LAB 14

![a](https://blog.pythian.com/wp-content/uploads/Untitled-2-350x219.png)

Ceci est la fin du projet qui va vous permettre de :
 * Connaître les caractéristiques et le fonctionnement d'Ansible
 * Savoir écrire des playbooks (scripts) pour orchestrer des opérations
 * Comprendre comment tirer parti de la solution pour optimiser le pilotage d’un parc et le déploiement d’applications

Ce LAB 14 permet d'effectuer les tâches décrites aux LABS 1 à 4, non plus localement avec Vagrant-Ansible, mais cette fois-ci, dans le cloud avec **Terraform-Ansible**. 

Le fournisseur DigitalOcean permet d'obtenir un compte de démonstration GRATUIT pendant 30 jours grâce au code promo:  **ACTIVATE10** 

Rappel du contenu des LABS 1 à 4:
  * LAB #1  - Création/Importation de vos Machines Virtuelles,
  * LAB #2  - Connexion à vos Machines Virtuelles,
  * LAB #3  - Installation d'Ansible,
  * LAB #4  - Configuration de SSH et de sudo,

## Pour commencer

Ce projet a été réalisé avec un ordinateur disposant de 16Go de RAM d'au moins 60 Go d'espace disque sous UBUNTU 16.04 LTS afin d'être à l'aise.

### Pré-requis

Ce qui est requis pour commencer avec ce projet **Ansible-Terraform**...

- avoir réalisé le projet Ansible-Vagrant : 
git clone https://framagit.org/ericlegrandformation/ansible.git

- avoir créer un compte **Digital Ocean** et activer le code Promo:
[https://cloud.digitalocean.com/registrations/new?onboarding_origin=kubernetes](https://cloud.digitalocean.com/registrations/new?onboarding_origin=kubernetes)
-  avoir réalisé la configuration de votre compte Digital Ocean, notamment,
créer le **Token** API Digital Ocean et la **clé ssh** pour accéder à Digital Ocean Cloud sans mot de passe: [https://www.digitalocean.com/community/tutorials/how-to-use-terraform-with-digitalocean](https://www.digitalocean.com/community/tutorials/how-to-use-terraform-with-digitalocean)

### Installation

Les 13 LABS sont prévus pour être réalisés à partir de **VM VirtualBox non disponible** dans ce projet.
Les LABs 5 à 13 seront à réaliser à partir des VM fournies.
 
Cependant, nous avons réalisé une première alternative en re-créant nos VM localement au cours du projet Ansible-Vagrant https://framagit.org/ericlegrandformation/ansible.git.

Ce LAB 14 bonus permet de déployer l'infrastructure mise au point localement cette fois-ci directement dans le cloud (nous utiliserons Digital Ocean).

## Démarrage

Vous êtes donc dans la dernière phase de l'approche qui consiste à créer, avec **Terraform et Ansible**, l'infrastructure dont vous allez avoir besoin.

Sur votre machine hôte, depuis votre répertoire **/home/'votre nom utilisateur'**, dans un terminal, taper successivement :
* **git clone** **https://framagit.org/ericlegrandformation/ansible-digitalocean.git** 
* ouvrer le dossier "ansible-digitalocean" **avec VSCode**
* ouvrer un **nouveau terminal** dans VSCode
* créer un environnement virtuel python avec la commande "**virtualenv --python=python3.5 venv**"
* activer l'environnement virtuel python avec la commande "**source ./venv/bin/activate**"
* installer quelques pré-requis avec la commande "**sudo apt install libffi-dev python-dev libssl-dev**"
* installer ansible avec la commande "**pip install ansible**"
* vérifier la version ansible avec "**ansible --version**" (version 2.8 mini)

Procéder à l'installation de **Terraform**:
* Terraform n'est pas disponible sous forme de dépôt ubuntu/debian. Pour l'installer il faut le télécharger et l'installer manuellement:
$ cp /tmp
$ wget https://releases.hashicorp.com/terraform/0.12.6/terraform_0.12.6_linux_amd64.zip
$ sudo unzip ./terraform_0.12.6_linux_amd64.zip -d /usr/local/bin/
* puis tester l'installation:
$ terraform --version


Nous aurons aussi besoin d'un outil supplémentaire pour pouvoir faire un **inventaire dynamique**:
* suivez les instructions d'installation : [https://github.com/nbering/terraform-provider-ansible/](https://github.com/nbering/terraform-provider-ansible/)
 
Téléchargez également ce script qui servira effectivement d'inventaire:
-     `wget https://raw.githubusercontent.com/nbering/terraform-inventory/master/terraform.py`

Tous les outils sont installés, il faut procéder à la configuration maintenant. L'enregistrement du token et de la clé ssh dans le fichier terraform.tfvars:
* copiez le fichier  `terraform.tfvars.dist`  et renommez le en enlevant le  `.dist`
* rendez vous sur votre compte Digital Ocean pour récupérer le **token** et copiez le hash du token
* collez le token dans le fichier de variables  `terraform.tfvars` entre les guillemets de la ligne: do_api_token  = " "
* récupérer la signature de votre **clé ssh** :
$ cd /home/"utilisateur"/.ssh
$ ssh-keygen -E md5 -lf ~/.ssh/id_rsa.pub | awk '{print $2}'
* et ajoutez le code retourné par la commande ci dessus (sans le MD5:), entre les guillemets de la ligne: do_sshkey_id  =  " "

Procéder à la création des serveurs:
- Lancer la commande :  "**$ terraform init**"
- Lancer la commande :  "**$ terraform apply**"

Vérifier le bon fonctionnement des serveurs:
- $ ansible ansible_nodes -m ping
- Testez l'inventaire dynamique : `chmod +x terraform.py && ./terraform.py`. Vous devriez avoir du texte JSON en retour de ce programme.

Terminer la configuration des serveurs en executant les **playbook Ansible**:
* Configurer le serveur Ansible avec la commande "**$ ansible-playbook setup_ansible.yml**"
* Configurer les serveurs Target 0 et Target 1 avec la commande "**$ ansible-playbook setup_target.yml**"
* Metter à jour les fichiers /etc/hosts des 3 machines avec "**$ ansible-playbook updatehosts.yml**"
* Configurer l'accès sécuriser ssh des 3 machines avec "**$ ansible-playbook sshkeys-exchange**"

Vérifier la configuration des serveurs:
* Vérifier la configuration des 3 machines en visualisant tous les fichiers utiles après vous être connecter en ssh avec les commandes "**ssh root@'ip_serveur**" 



*Bravo, vous venez de faire les LABs 1 à 4 dans le cloud.*

***

## Versions

**Dernière version stable :** 0.1
**Dernière version :** 0.1
Liste des versions : [Cliquer pour afficher](https://framagit.org/ericlegrandformation/ansible.git)

## Auteurs

* **Eric Legrand** _alias_ [@ericlegrand](https://framagit.org/ericlegrandformation/ansible.git)
avec le soutien de Messieurs Hugh Norris et Elie Gavoty.
