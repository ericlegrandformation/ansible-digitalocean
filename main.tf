variable "do_api_token" {}
variable "do_sshkey_id" {}

provider "digitalocean" {
    token = "${var.do_api_token}"
}

# Variables
locals {
  ansible_master_count = 1 
  ansible_target_count = 2
}

resource "digitalocean_droplet" "masters" {
  count = "${local.ansible_master_count}"
  name = "ansible-master-${count.index}"
  image = "ubuntu-18-04-x64"
  size = "2gb"
  region = "fra1"
  ssh_keys = ["${var.do_sshkey_id}"]
}

resource "digitalocean_droplet" "targets" {
  count = "${local.ansible_target_count}"
  name = "ansible-target-${count.index}"
  image = "ubuntu-18-04-x64"
  size = "1gb"
  region = "fra1"
  ssh_keys = ["${var.do_sshkey_id}"]
}

## Ansible mirroring hosts section
# Using https://github.com/nbering/terraform-provider-ansible/ to be installed manually (third party provider)
# Copy binary to ~/.terraform.d/plugins/

resource "ansible_host" "ansible_masters" {
  count = "${local.ansible_master_count}"
  inventory_hostname = "master-${count.index}"
  groups = ["ansible_nodes"]
  vars = {
    ansible_host = "${element(digitalocean_droplet.masters.*.ipv4_address, count.index)}"
  }
}

resource "ansible_host" "ansible_targets" {
  count = "${local.ansible_target_count}"
  inventory_hostname = "target-${count.index}"
  groups = ["ansible_nodes"]
  vars = {
    ansible_host = "${element(digitalocean_droplet.targets.*.ipv4_address, count.index)}"
  }
}

resource "ansible_group" "all" {
  inventory_group_name = "all"
  vars = {
    ansible_python_interpreter = "/usr/bin/python3"
  }
}
